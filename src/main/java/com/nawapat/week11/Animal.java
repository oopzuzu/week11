package com.nawapat.week11;

public abstract class Animal {
    private String name;
    private int numOfleg;

    public Animal(String name, int numOfleg) {
        this.name = name;
        this.numOfleg = numOfleg;
    }

    public String getName() {
        return this.name;
    }

    public int getNumOfLeg() {
        return this.numOfleg;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNumOfLeg(int numOfleg) {
        this.numOfleg = numOfleg;
    }

    @Override
    public String toString() {
        return "Animal(" + name + ")";
    }

    public abstract void sleep();

    public abstract void eat();

}
